Categories:Money
License:GPL-3.0
Web Site:https://btcontract.com
Source Code:https://github.com/btcontract/wallet
Issue Tracker:https://github.com/btcontract/wallet/issues

Auto Name:Bitcoin
Summary:Simple Bitcoin wallet
Description:
Simple Bitcoin is a standalone wallet for Android devices which does not depend
on any centralized service and gives you full control over your money.
.

Repo Type:git

Repo:https://github.com/btcontract/wallet.git

Build:1.075,53
    commit=3003b15beb922385b11a8c78fc28c82f095b144a
    subdir=app
    submodules=yes
    gradle=yes
    scandelete=app/src/main/jniLibs/armeabi-v7a/libscrypt.so,app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=pushd $$jbox2d$$/jbox2d-library && \
        $$MVN3$$ install && \
        popd && \
        ./prepare_fdroid.sh

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.075
Current Version Code:53
